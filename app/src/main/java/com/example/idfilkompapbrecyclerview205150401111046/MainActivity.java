package com.example.idfilkompapbrecyclerview205150401111046;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static String TAG = "RV1";
    RecyclerView rv1;
    Button btSimpan;
    EditText etNama, etNama2;
    MahasiswaAdapter mahasiswaAdapter;
    ArrayList<Mahasiswa> mahasiswaList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv1 = findViewById(R.id.rv1);
        rv1.setHasFixedSize(true);

        btSimpan = findViewById(R.id.btSimpan);
        etNama = findViewById(R.id.etNama);
        etNama2 = findViewById(R.id.etNama2);


        btSimpan.setOnClickListener(v ->{
            mahasiswaList.add(new Mahasiswa(etNama.getText().toString(), etNama2.getText().toString()));
            mahasiswaAdapter = new MahasiswaAdapter(this,mahasiswaList);
            rv1.setAdapter(mahasiswaAdapter);
        });
        mahasiswaList.add(new Mahasiswa("2051504011110111", "Yulis"));
        mahasiswaList.add(new Mahasiswa("205150401111023", "Reza"));
        mahasiswaAdapter = new MahasiswaAdapter(this, mahasiswaList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        rv1.setLayoutManager(layoutManager);

        mahasiswaAdapter.setOnItemClickistener((position, v) -> {
            mahasiswaAdapter = new MahasiswaAdapter(this,mahasiswaList);
            rv1.setAdapter(mahasiswaAdapter);
        });
        rv1.setAdapter(mahasiswaAdapter);

//      ArrayList<Mahasiswa> data = getData();
//        MahasiswaAdapter adapter = new MahasiswaAdapter(this, data);
//        rv1.setAdapter(adapter);
//        rv1.setLayoutManager(new LinearLayoutManager(this));
    }
//
//    public ArrayList getData() {
//        ArrayList<Mahasiswa> data = new ArrayList<>();
//        List<String> nim = Arrays.asList(getResources().getStringArray(R.array.nim));
//        List<String> nama = Arrays.asList(getResources().getStringArray(R.array.nama));
//        for (int i = 0; i < nim.size(); i++) {
//            Mahasiswa mhs = new Mahasiswa();
//            mhs.nim = nim.get(i);
//            mhs.nama = nama.get(i);
//            Log.d(TAG,"getData "+mhs.nim);
//            data.add(mhs);
//        }
//        return data;
//    }

}